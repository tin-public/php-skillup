# Php-challenge

## [Php 7 features](./Php7.md)

## [PSR](./Psr.md)

## Source for test array, string and json functions
### How to run test
```
cd ./devcontainer
docker build -t phpchallenge .
cd ..
docker run --rm -v $(pwd):/src phpchallenge composer i -d /src
docker run --rm -v $(pwd):/src phpchallenge /src/vendor/bin/phpunit --testdox /src/tests
```

### Content
#### Array functions
- array_merge_recursive
- array_multi_sort
- array_diff
- array_chunk
- array_uintersect

#### String functions
- htmlspecialchars
- levenshtein
- parse_str
- strspn
- strtr

#### Json functions
- json_decode
- json_encode
