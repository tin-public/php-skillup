# 7.0
## Scalar type declarations
```php
<?php
// Coercive mode
function sumOfInts(int ...$ints)
{
    return array_sum($ints);
}

var_dump(sumOfInts(2, '3', 4.1)); // 9
```
Strict mode
```php
<?php
// Strict mode
declare(strict_types = 1);

function sumOfInts(int ...$ints)
{
    return array_sum($ints);
}

var_dump(sumOfInts(2, '3', 4.1)); // Error
```
## Return type declarations
```php
<?php

function arraysSum(array ...$arrays): array
{
    return array_map(function(array $array): int {
        return array_sum($array);
    }, $arrays);
}

var_dump(arraysSum([1, 2, 3], [4, 5, 6], [7, 8, '9.9'])); // [6, 15, 24]
```
## Null coalescing operator
```php
<?php
// Fetches the value of $_GET['user'] and
// returns 'nobody' if it does not exist.
$username = $_GET['user'] ?? 'nobody';
// This is equivalent to:
$username = isset($_GET['user']) ? $_GET['user'] : 'nobody';

// Coalescing can be chained: this will return the first
// defined value out of $_GET['user'], $_POST['user'], and
// 'nobody'.
$username = $_GET['user'] ?? $_POST['user'] ?? 'nobody';
```
## Spaceship operator
```php
<?php
// Integers
echo 1 <=> 1; // 0
echo 1 <=> 2; // -1
echo 2 <=> 1; // 1

// Floats
echo 1.5 <=> 1.5; // 0
echo 1.5 <=> 2.5; // -1
echo 2.5 <=> 1.5; // 1

// Strings
echo "a" <=> "a"; // 0
echo "a" <=> "b"; // -1
echo "b" <=> "a"; // 1
```
## Constant arrays using `define()`
Array constants can now be defined with `define()`. In PHP 5.6, they could only be defined with `const`.
```php
<?php
define('ANIMALS', [
    'dog',
    'cat',
    'bird'
]);

echo ANIMALS[1]; // outputs "cat"
```
## Anonymous classes
```php
<?php
interface Logger {
    public function log(string $msg);
}

class Application {
    private $logger;

    public function getLogger(): Logger {
      return $this->logger;
    }

    public function setLogger(Logger $logger) {
        $this->logger = $logger;
    }
}

$app = new Application;
// Anonymous class
$app->setLogger(new class implements Logger {
    public function log(string $msg) {
        echo $msg;
    }
});

var_dump($app->getLogger());
```
## Unicode codepoint escape syntax
```php
echo "\u{aa}";  // ª
echo "\u{0000aa}";  // ª (same as before but with optional leading 0's)
echo "\u{9999}"; // 香
```
## `Closure::call()`
```php
<?php
class A {private $x = 1;}

// Pre PHP 7 code
$getX = function() {return $this->x;};
$getXCB = $getX->bindTo(new A, 'A'); // intermediate closure
echo $getXCB();

// PHP 7+ code
$getX = function() {return $this->x;};
echo $getX->call(new A);
```

## Group `use` declarations
```php
<?php
// Pre PHP 7 code
use some\namespace\ClassA;
use some\namespace\ClassB;
use some\namespace\ClassC as C;

use function some\namespace\fn_a;
use function some\namespace\fn_b;
use function some\namespace\fn_c;

use const some\namespace\ConstA;
use const some\namespace\ConstB;
use const some\namespace\ConstC;

// PHP 7+ code
use some\namespace\{ClassA, ClassB, ClassC as C};
use function some\namespace\{fn_a, fn_b, fn_c};
use const some\namespace\{ConstA, ConstB, ConstC};
```
## Integer division with `intdiv()`
```php
<?php
var_dump(intdiv(10, 3));  // 3
```

# 7.1
## Nullable types
```php
<?php

function testReturn(): ?string
{
    // or return null;
    return 'elePHPant';
}

function test(?string $name)
{
    var_dump($name);
}

test('elePHPant'); // elePHPant
test(null); // NULL
test(); // throw ArgumentCountError
```
## Void functions
```php
<?php
function swap(&$left, &$right): void
{
    if ($left === $right) {
        return;
    }

    $tmp = $left;
    $left = $right;
    $right = $tmp;
}

$a = 1;
$b = 2;
var_dump(swap($a, $b), $a, $b); // NULL 2 1
```
## Symmetric array destructuring
```php
<?php
$data = [
    [1, 'Tom'],
    [2, 'Fred'],
];

// list() style
list($id1, $name1) = $data[0];

// [] style
[$id1, $name1] = $data[0];

// list() style
foreach ($data as list($id, $name)) {
    // logic here with $id and $name
}

// [] style
foreach ($data as [$id, $name]) {
    // logic here with $id and $name
}
```
## Support for keys in list()
```php
<?php
$data = [
    ["id" => 1, "name" => 'Tom'],
    ["id" => 2, "name" => 'Fred'],
];

// list() style
list("id" => $id1, "name" => $name1) = $data[0];

// [] style
["id" => $id1, "name" => $name1] = $data[0];

// list() style
foreach ($data as list("id" => $id, "name" => $name)) {
    // logic here with $id and $name
}

// [] style
foreach ($data as ["id" => $id, "name" => $name]) {
    // logic here with $id and $name
}
```

## Class constant visibility
```php
<?php
class ConstDemo
{
    const PUBLIC_CONST_A = 1;
    public const PUBLIC_CONST_B = 2;
    protected const PROTECTED_CONST = 3;
    private const PRIVATE_CONST = 4;
}
```
## Multi catch exception handling
```php
<?php
try {
    // some code
} catch (FirstException | SecondException $e) {
    // handle first and second exceptions
}
```
## Support for negative string offsets
```php
<?php
var_dump("abcdef"[-2]);  // string (1) "e"
var_dump(strpos("aabbcc", "b", -3)); // int(3)
$string = 'bar';
echo "The last character of '$string' is '$string[-1]'.\n";
// The last character of 'bar' is 'r'.
```

# 7.2
## New `object` type
```php
<?php

function test(object $obj) : object
{
    var_dump($obj);
    return new DateTime($obj->birthdate);
}

// Object-styled definition
$person = new StdClass();
$person->name = 'A Unknown';
$person->birthdate = '1991-01-01 01:01:01';
test($person);

$person_array = [
    'name' => 'B Unknown',
    'age' => '1999-09-09 09:09:09',
];
// Type casting from array to object
$person2 = (object)$person_array;
test($person2);
```

## Abstract method overriding
```php
<?php

abstract class A
{
    abstract function test(string $s);
}
abstract class B extends A
{
    // overridden - still maintaining contravariance for parameters and covariance for return
    abstract function test($s) : int;
}
```
## Parameter type widening
```php
<?php

interface A
{
    public function Test(array $input);
}

class B implements A
{
    public function Test($input){} // type omitted for $input
}
```
## Allow a trailing comma for grouped namespaces
```php
<?php

use Foo\Bar\{
    Foo,
    Bar,
    Baz,
};
```

# 7.3
## More Flexible Heredoc and Nowdoc Syntax
```php
<?php
function test()
{
    $str = <<<EOD
    Example of string
    spanning multiple lines
    using heredoc syntax.
    EOD; // can not on pre 7.3

    var_dump($str);
}

test();
/*
string(65) "Example of string
spanning multiple lines
using heredoc syntax."
*/
```
Pre 7.3
```php
<?php
function test()
{
    $str = <<<EOD
Example of string
spanning multiple lines
using heredoc syntax.
EOD;

    var_dump($str);
}

test();
```
## Array Destructuring supports Reference Assignments
```php
<?php
[&$a, [$b, &$c]] = $d
```
Example
```php
<?php
$d = ['a', ['b', 'c']];
[&$a, [$b, &$c]] = $d;

$a = 'ar'; $b = 'br'; $c = 'cr';

var_dump($d);
/*
array(2) {
  [0]=> &string(2) "ar"
  [1]=> array(2) {
    [0]=> string(1) "b"
    [1]=> &string(2) "cr"
  }
}
*/
```
## Trailing Commas are allowed in Calls
```php
<?php
array_uintersect(
    [1, 2, 3],
    [2, 3, 4], // Trailing comma
)
```

# 7.4
## Typed properties
```php
<?php
class User {
    public int $id; // Typed property
    public string $name; // Typed property
}
```
## Arrow functions
```php
<?php
$factor = 10;
$nums = array_map(fn($n) => $n * $factor, [1, 2, 3, 4]);
// $nums = array(10, 20, 30, 40);
```
## Null coalescing assignment operator
```php
<?php
$array['key'] ??= computeDefault();
// is roughly equivalent to
if (!isset($array['key'])) {
    $array['key'] = computeDefault();
}
```
## Unpacking inside arrays
```php
<?php
$parts = ['apple', 'pear'];
$fruits = ['banana', 'orange', ...$parts, 'watermelon'];
// ['banana', 'orange', 'apple', 'pear', 'watermelon'];
```
## Numeric literal separator
```php
<?php
// Numeric literals can contain underscores between digits.
6.674_083e-11; // float
299_792_458; // decimal
0xCAFE_F00D; // hexadecimal
0b0101_1111; // binary
```

