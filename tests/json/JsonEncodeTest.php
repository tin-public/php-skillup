<?php
use PHPUnit\Framework\TestCase;

/**
 * json_encode ( mixed $value [, int $options = 0 [, int $depth = 512 ]] ) : string
 * Returns the JSON representation of a value
 */
final class JsonEncodeTest extends TestCase
{
    public function testWithInputIsIndexArray(): void
    {

        $input = [false, true, 0, '0',null, '', 1, 'string'];

        $expected = '[false,true,0,"0",null,"",1,"string"]';

        $actual = json_encode($input);

        $this->assertEquals($expected, $actual);
    }

    public function testWithInputIsAssociativeArray(): void
    {

        $input = [
            'a' => true,
        ];

        $expected = '{"a":true}';

        $actual = json_encode($input);

        $this->assertEquals($expected, $actual);
    }

    public function testWithInputIsObject(): void
    {

        $input = (object)[
            'a' => true,
        ];

        $expected = '{"a":true}';

        $actual = json_encode($input);

        $this->assertEquals($expected, $actual);
    }

    public function testWithSomeOptionFlags(): void
    {
        $input = [
            'hello' => '今日は'
        ];

        $expected = <<<'JSON'
        {
            "hello": "今日は"
        }
        JSON;

        // Test with some option flags to change format of output string
        // JSON_PRETTY_PRINT
        // JSON_UNESCAPED_UNICODE
        $actual = json_encode($input, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        $this->assertEquals($expected, $actual);
    }
}
