<?php
use PHPUnit\Framework\TestCase;

/**
 * json_decode ( string $json [, bool $assoc = FALSE [, int $depth = 512 [, int $options = 0 ]]] ) : mixed
 * Decodes a JSON string
 */
final class JsonDecodeTest extends TestCase
{
    public function testWithDefaultParamsReturnObject(): void
    {
        $input = <<<'JSON'
        {
            "a": true
        }
        JSON;

        $expected = (object)[
            'a' => true,
        ];

        $actual = json_decode($input);

        $this->assertEquals($expected, $actual);
    }

    public function testReturnArray(): void
    {
        $input = <<<'JSON'
        {
            "a": true
        }
        JSON;

        $expected = [
            'a' => true,
        ];

        $actual = json_decode($input, true);

        $this->assertEquals($expected, $actual);
    }

    public function testJsonDepthLargerThanDepthParam(): void
    {
        /**
         * { // 1
         *   "grand": { // 2
         *       "parent": { // 3
         *           "son": 3, // 4
         *           "daughter": 2
         *       }
         *   }
         * }
         */
        $input = <<<'JSON'
        {
            "grand": {
                "parent": {
                    "son": 3,
                    "daughter": 2
                }
            }
        }
        JSON;

        $expected = null;

        // json depth is 4 meanwhile limit depth is 3, therefore json_decode return null
        $actual = json_decode($input, true, 3);

        $this->assertEquals($expected, $actual);
    }

    public function testJsonDepthEqualDepthParam(): void
    {
        /**
         * { // 1
         *   "grand": { // 2
         *       "parent": { // 3
         *           "son": 3, // 4
         *           "daughter": 2
         *       }
         *   }
         * }
         */
        $input = <<<'JSON'
        {
            "grand": {
                "parent": {
                    "son": 3,
                    "daughter": 2
                }
            }
        }
        JSON;

        $expected = [
            'grand' => [
                'parent' => [
                    'son' => 3,
                    'daughter' => 2,
                ]
            ],
        ];

        // json depth is equal limit depath, therefore json_decode return result
        $actual = json_decode($input, true, 4);

        $this->assertEquals($expected, $actual);
    }


    public function testCatchInvalidJson(): void
    {
        $input = <<<'JSON'
        {
            this: does not is a json string
        }
        JSON;

        try {
            // Default json_decode will return null if it can not decode json string
            // use 4th param to throw JsonException if decode failed
            $actual = json_decode($input, true, 512, JSON_THROW_ON_ERROR);
            $exception = null;
        } catch (\Throwable $exception) {
        }

        $this->assertInstanceOf(JsonException::class, $exception);
    }
}
