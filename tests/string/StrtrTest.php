<?php
use PHPUnit\Framework\TestCase;

/**
 * strtr ( string $str , string $from , string $to ) : string
 * strtr ( string $str , array $replace_pairs ) : string
 * Translate characters or replace substrings
 */
final class StrtrTest extends TestCase
{
    public function testBasic(): void
    {
        $input = 'hello Halo!';

        $expected = '4e!!o H@!o!';

        // in $input, h is replaced by 4, a => @, l => !
        // H is not replaced because of case sensitive
        $actual = strtr($input, 'hal', '4@!');

        $this->assertEquals($expected, $actual);
    }

    public function testUsingReplacePairsCase(): void
    {
        $input = 'hi all, I said hello';

        $expected = 'hello all, I said hi';

        $trans = ['h' => '-', 'hello' => 'hi', 'hi' => 'hello'];

        // Note the preference of the replacements ("h" is not picked because there are longer matches)
        // and how replaced text was not searched again.
        $actual = strtr($input, $trans);

        $this->assertEquals($expected, $actual);
    }
}
