<?php
use PHPUnit\Framework\TestCase;

/**
 * htmlspecialchars ( string $string [, int $flags = ENT_COMPAT | ENT_HTML401 [, string $encoding = ini_get("default_charset") [, bool $double_encode = TRUE ]]] ) : string
 * Convert special characters to HTML entities
 */
final class HtmlspecialcharsTest extends TestCase
{
    public function testBasic(): void
    {
        $input = '<a href="https://a.io" target=\'_blank\'>';

        $expected = '&lt;a href=&quot;https://a.io&quot; target=\'_blank\'&gt;';

        // The default flag ENT_COMPAT will convert double-quotes and leave single-quotes alone.
        // More detail https://www.php.net/manual/en/function.htmlspecialchars.php
        $actual = htmlspecialchars($input);

        $this->assertEquals($expected, $actual);
    }
}
