

<?php
use PHPUnit\Framework\TestCase;

/**
 * strspn ( string $subject , string $mask [, int $start [, int $length ]] ) : int
 * Finds the length of the initial segment of a string consisting entirely of characters contained within a given mask
 */
final class StrspnTest extends TestCase
{
    public function testParseStrAllSubject(): void
    {
        $test_string = '10*123=1230';

        $expected = 2;

        $actual = strspn($test_string, '0123456789');

        $this->assertEquals($expected, $actual);
    }
    public function testParseStrWithStart(): void
    {
        //              01234567
        $test_string = '10*123=1230';

        $expected = 4;

        $actual = strspn($test_string, '0123456789', 7);

        $this->assertEquals($expected, $actual);
    }
}
