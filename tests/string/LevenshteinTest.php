<?php
use PHPUnit\Framework\TestCase;

/**
 * levenshtein ( string $str1 , string $str2 ) : int
 * Calculate Levenshtein distance between two strings
 */
function findSimilarWord(string $input, array $words) : string
{
    // no shortest distance found, yet
    $shortest = -1;

    // loop through words to find the closest
    foreach ($words as $word) {

        // calculate the distance between the input word and the current word
        $lev = levenshtein($input, $word);

        // check for an exact match
        if ($lev == 0) {
            return $word;
        }

        // if this distance is less than the next found shortest distance
        // OR if a next shortest word has not yet been found
        if ($lev <= $shortest || $shortest < 0) {
            // set the closest match, and shortest distance
            $closest  = $word;
            $shortest = $lev;
        }
    }

    return $closest;
}

final class LevenshteinTest extends TestCase
{
    public function testFindSimilarWordOk1(): void
    {
        $input = 'glue';
        $colors = ['red', 'green', 'blue', 'white', 'black'];

        $expected = 'blue';

        $actual = findSimilarWord($input, $colors);

        $this->assertEquals($expected, $actual);
    }

    public function testFindSimilarWordOk2(): void
    {
        $input = 'gren';
        $colors = ['red', 'green', 'blue', 'white', 'black'];

        $expected = 'green';

        $actual = findSimilarWord($input, $colors);

        $this->assertEquals($expected, $actual);
    }
}
