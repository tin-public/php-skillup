<?php
use PHPUnit\Framework\TestCase;

/**
 * parse_str ( string $encoded_string [, array &$result ] ) : void
 * Parses URL query string into variables
 */
final class ParseStrTest extends TestCase
{
    public function testParseStr(): void
    {
        $query_string = 'favorite=transparent&colors[]=white&colors[]=black&none';

        $expected = [
            'favorite' => 'transparent',
            'colors' => ['white', 'black'],
            'none' => '',
        ];

        parse_str($query_string, $actual);

        $this->assertEquals($expected, $actual);
    }
}
