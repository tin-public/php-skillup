<?php
use PHPUnit\Framework\TestCase;

/**
 * array_diff ( array $array1 , array $array2 [, array $... ] ) : array
 * Compare only value
 * Two elements are considered equal if and only if (string) $elem1 === (string) $elem2.
 * Array keys are preserved.
 */
final class ArrayDiffTest extends TestCase
{
    public function testDiffWithAssociativeArray(): void
    {
        $ar1 = [
            1,
            'key_same' => 'same',
            2,
            'key_diff' => 'diff',
            'value_same' => 'value_same',
            3,
        ];
        $ar2 = [
            5,
            '2',
            'key_same' => 'same',
            'key_diff' => 'bla',
            'value_diff' => 'value_same',
            1,
        ];

        $expected = [
            'key_diff' => 'diff',
            2 => 3,
        ];

        $actual = array_diff($ar1, $ar2);

        $this->assertEquals($expected, $actual);
    }
}
