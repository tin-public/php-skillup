<?php
use PHPUnit\Framework\TestCase;

/**
 * array_multisort ( array &$array1 [, mixed $array1_sort_order = SORT_ASC [, mixed $array1_sort_flags = SORT_REGULAR [, mixed $... ]]] ) : bool
 */
final class ArrayMultisortTest extends TestCase
{
    public function testMultisortBasic(): void
    {
        $ar1 = [3, 4, 2, 1];
        $ar2 = ['three', 'four', 'two', 'one'];
        $ar3 = ['三', '四', '二', '一'];

        $expected = [
            [1, 2, 3, 4],
            ['one', 'two', 'three', 'four'],
            ['一', '二', '三', '四']
        ];

        array_multisort($ar1, $ar2, $ar3);
        $actual = [
            $ar1,
            $ar2,
            $ar3,
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testMultisortWithNaturalAndDescending(): void
    {
        $ar1 = ['file1', 'file2', 'file10', 'file3'];
        $ar2 = [1, 2, 10, 3];

        $expected = [
            ['file10', 'file3', 'file2', 'file1'],
            [10, 3, 2, 1],
        ];

        array_multisort($ar1, SORT_DESC, SORT_NATURAL, $ar2);
        $actual = [
            $ar1,
            $ar2,
        ];

        $this->assertEquals($expected, $actual);
    }
}
