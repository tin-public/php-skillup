<?php
use PHPUnit\Framework\TestCase;

/**
 * array_uintersect ( array $array1 , array $array2 [, array $... ], callable $value_compare_func ) : array
 * 
 * Computes the intersection of arrays, compares data by a callback function
 * Compare only value
 * Array keys are preserved.
 * Callback return interger
 */
final class ArrayUIntersectTest extends TestCase
{
    public function testBasic(): void
    {
        $ar1 = [
            'a' => 'green',
            'b' => 'brown',
            'c' => 'blue',
            'fushica',
            'red',
        ];
        $ar2 = [
            'a' => 'GREEN',
            'B' => 'brown',
            'yellow',
            'red',
            'blUe'
        ];

        $expected = [
            'a' => 'green',
            'b' => 'brown',
            'c' => 'blue',
            1 => 'red',
        ];
        $actual = array_uintersect($ar1, $ar2, function($v1, $v2) {
            $v1 = strtolower($v1);
            $v2 = strtolower($v2);
            // It doesn't work if use this return
            // return $v1 === $v2 ? 0 : 1;
            if ($v1 === $v2) return 0;
            if ($v1 > $v2) return 1;
            return -1;
        });

        $this->assertEquals($expected, $actual);
    }
}
