<?php
use PHPUnit\Framework\TestCase;

/**
 * array_uintersect ( array $array1 , array $array2 [, array $... ], callable $value_compare_func ) : array
 * 
 * Merge one or more arrays recursively
 * If the input arrays have the same string keys, then the values for these keys are merged together into an array.
 * If the arrays have the same numeric key, the later value will not overwrite the original value, but will be appended.
 */
final class ArrayMergeRecursiveTest extends TestCase
{
    public function testBasic(): void
    {
        $ar1 = [
            1,
            'color' => [
                'favorite' => 'green',
                'blue',
            ],
            6
        ];
        $ar2 = [
            'color' => [
                'white',
                'favorite' => [
                    'black',
                    'fushica',
                ],
            ],
            2,
            5
        ];

        $expected = [
            1,
            'color' => [
                'favorite' => [
                    'green',
                    'black', // added
                    'fushica', // added
                ],
                'blue',
                'white', // added
            ],
            6,
            2, // added
            5, // added
        ];
        $actual = array_merge_recursive($ar1, $ar2);

        $this->assertEquals($expected, $actual);
    }
}
