<?php
use PHPUnit\Framework\TestCase;

/**
 * array_filter ( array $array [, callable $callback [, int $flag = 0 ]] ) : array
 * 
 * Filters elements of an array using a callback function
 * If the callback function returns TRUE, the current value from array is returned into the result array.
 * If no callback is supplied, all entries of array equal to FALSE (see converting to boolean) will be removed.
 * Flag determining what arguments are sent to callback: Default pass value
 */
final class ArrayFilterTest extends TestCase
{
    public function testFilterElementHasValueIsEnv(): void
    {
        $ar = [
            1,
            2,
            'index3' => 3,
            'index4' => 4,
            5,
            6
        ];

        $expected = [
            1 => 2,
            'index4' => 4,
            3 => 6,
        ];

        $actual = array_filter($ar, function($value){
            return $value % 2 === 0;
        });

        $this->assertEquals($expected, $actual);
    }

    /**
     * If no callback is supplied, all entries of array equal to FALSE (see converting to boolean) will be removed.
     */
    public function testWithNoCallback(): void
    {
        $ar = [
            'foo',
            false,
            -1,
            null,
            '',
            '0',
            0,
            [],
        ];

        $expected = [
            0 => 'foo',
            2 => -1,
        ];

        $actual = array_filter($ar);

        $this->assertEquals($expected, $actual);
    }

    public function testWithFlagFilterElementIsEvenAndIntegerKey(): void
    {
        $ar = [
            1,
            2,
            'index3' => 3,
            'index4' => 4,
            5,
            6
        ];

        $expected = [
            1 => 2,
            3 => 6,
        ];


        $actual = array_filter(
            $ar,
            fn($value, $key) => is_int($key) && $value % 2 === 0,
            ARRAY_FILTER_USE_BOTH,
        );

        $this->assertEquals($expected, $actual);
    }
}
