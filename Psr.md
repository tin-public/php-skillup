# PSR?
PSR stands for PHP Standard Recommendation.
PSR serves the standardization of programming concepts in PHP. The aim is to enable interoperability of components and to provide a common technical basis for implementation of proven concepts for optimal programming and testing practices.

# Accepted versions
Version/Number|Title
--|--
1|Basic Coding Standard
3|Logger Interface
4|Autoloading Standard
6|Caching Interface
7|HTTP Message Interface
11|Container Interface
12|Extended Coding Style Guide
13|Hypermedia Links
14|Event Dispatcher
15|HTTP Handlers
16|Simple Cache
17|HTTP Factories
18|HTTP Client

# PSR-12: Extended Coding Style Guide
## File
- Files MUST use only <?php and <?= tags. (<?)
- Files MUST use only UTF-8 without BOM for PHP code.
- MUST use the Unix LF (linefeed) line ending only.
- The closing ?> tag MUST be omitted from files containing only PHP.
- Files MUST end with a non-blank line.
- Files SHOULD either declare symbols (classes, functions, constants, etc.) or cause side-effects (e.g. generate output, change .ini settings, etc.) but SHOULD NOT do both.

```php
<?php

class Foo
{
    public function bar(): void
    {
        // Code
    }
}
```

## Space
- MUST NOT be trailing whitespace at the end of lines.
- MUST NOT be more than one statement per line.
- MUST use an indent of 4 spaces for each indent level, and MUST NOT use tabs for indenting.
- There MUST NOT be a space after the opening parenthesis/brace, and there MUST NOT be a space before the closing parenthesis/brace.
- There MUST NOT be a space between the method or function name and the opening parenthesis
- There MUST NOT be a space before each comma, and there MUST be one space after each comma.

## New line
- Lines SHOULD NOT be longer than 80 characters; lines longer than that SHOULD be split into multiple subsequent lines of no more than 80 characters each.
- Lists of implements and, in the case of interfaces, extends MAY be split across multiple lines

## Brace
- Opening brace for the class MUST go on its own line
- Opening braces MUST NOT be preceded or followed by a blank line.
- Closing braces MUST NOT be preceded by a blank line.
- Closing braces MUST be on their own line.
- Brace and control keyword (if, else, while, switch, ...) or closure MUST same line

## Naming
- Class names MUST be declared in StudlyCaps
- Class constants MUST be declared in all upper case with underscore separators. THIS_IS_CONSTANT
- Method names MUST be declared in camelCase.
- All PHP reserved keywords and types MUST be in lower case.
- Short form of type keywords MUST be used i.e. bool instead of boolean, int instead of integer etc.
- Name MUST NOT be prefixed with a single underscore to indicate protected or private visibility.

## Header
```php
<?php

/**
 * This file contains an example of coding styles.
 * - Header can consist of a number of different blocks
 * - If present, each of the blocks below MUST be separated by a single blank line, and MUST NOT contain a blank line
 * - Each block MUST be in the order listed below
 * - When a file contains a mix of HTML and PHP, any of the above sections may still be used
 * - Import statements MUST never begin with a leading backslash as they must always be fully qualified.
 */

declare(strict_types=1);

namespace Vendor\Package;

use Vendor\Package\{ClassA as A, ClassB, ClassC as C};
use Vendor\Package\SomeNamespace\ClassD as D;
use Vendor\Package\AnotherNamespace\ClassE as E;

use function Vendor\Package\{functionA, functionB, functionC};
use function Another\Vendor\functionD;

use const Vendor\Package\{CONSTANT_A, CONSTANT_B, CONSTANT_C};
use const Another\Vendor\CONSTANT_D;

/**
 * FooBar is an example class.
 */
class FooBar
{
    // ... additional PHP code ...
}
```

## Other
- When instantiating a new class, parentheses MUST always be present even when there are no arguments passed to the constructor. `new Foo();`
- The extends and implements keywords MUST be declared on the same line as the class name.
- Visibility MUST be declared on all properties, constants, properties.
- The var keyword MUST NOT be used to declare a property.
- ...
